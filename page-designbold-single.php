<?php
/**
 * Template Name: Design Bold single
 *
 */
 ?>
<?php
	$category = isset( $_GET['cat-id'] ) ? $_GET['cat-id'] : 0; 
	$keyword = isset( $_GET['q'] ) ? $_GET['q'] : null;
?>
<div class="row-header">
<div class="col-md-8 col-sm-12">
  <h2>DESIGNBOLD TEMPLATE</h2>
    <div class="sitemaps">
     <?php
if ( function_exists('yoast_breadcrumb') ) {
     yoast_breadcrumb('<p id="breadcrumbs">','</p>');
}
?>
    </div>
</div>
<div class="col-md-4 col-sm-12">
  <div class="help-search">
<div id="db-custom-search-box" class="col-md-12" data-page=<?php echo " http://blog.designbold.com/templates/"; ?>><input id="db-custom-search-input" style="" type="text" value="<?php echo $keyword; ?>" placeholder="Search..."><i id="db-custom-search-submit" aria-hidden="true"></i></div>
<input type="hidden" id="hidden-db-category" value="<?php echo $category; ?>">
                            </div>
                        </div>
                    </div>

<section id="primary" class="db-primary site-content">
		<div id="content" role="main" class="col-md-9 col-sm-8 col-xs-12">
			<?php 
				$post_id = isset( $_GET['post-id'] ) ? $_GET['post-id'] : null;
				if ( $post_id ) { ?>
					<div class="post-content">
						<?php if ( has_post_thumbnail( $post_id ) ) : ?>
							<div class="entry-thumbnail"><?php the_post_thumbnail( $post_id ); ?></div>
						<?php endif; ?>
						<div class="entry-content">
							<?php $post = get_post( $post_id ) ?>
							<?php echo $post->post_content; ?>
						</div>
						<?php
							$status = get_post_meta( $post_id, 'image-status', true );
							if ( $status && $status != '' ) {
								if ( 'paid' == $status ) { ?>
									<div class="entry-price">
										<?php $price = get_post_meta( $post_id, 'image-price', true ); ?>
										<h4>Price :</h4> <?php echo $price ?> $
									</div>
								<?php } else if ( 'free' == $status ) { ?>
									<div class="entry-price">
										<h4>Price</h4> free
									</div>
								<?php } ?>
							<?php }	?>
						<?php $tags = wp_get_post_tags( $post_id ); ?>
						<?php if ( $tags ) {
							$tag_arr = array();
							foreach ( $tags as $tag ) {
								$tag_arr[] = $tag->name;
							} ?>
							<div class="entry-tags">
                <h4>Keywords</h4>
								<?php echo implode( ', ' , $tag_arr); ?>
							</div>
						<?php } ?> 
					</div>
				<?php }
			?>
			<div class="related-post">
					<?php
					if ( $tag_arr ) {
						$args=array(
						'tag' => $tag_arr,
						'post__not_in' => array($post_id),
						'posts_per_page'=>3,
						'caller_get_posts'=>1,
						'orderby' => 'rand',
						);
					$my_query = new WP_Query($args);
					if( $my_query->have_posts() ) {
            ?>
            <h4>Related Layout</h4>
						<?php
					while ($my_query->have_posts()) : $my_query->the_post(); ?>
					<div class="col-md-4 template-item">
					<div class="entry-thumbnail db-thumbnail">
						<a href="http://blog.designbold.com/template-item/?post-id=<?php echo get_the_ID(); ?>"><?php echo db_unique_content(); ?></a>
					
					<div class="db-title entry-title"><?php echo get_the_title(); ?></div>
          <div class="entry-status">
					<?php $status = get_post_meta( get_the_ID(), 'image-status', true );
						if ( $status && $status != '' ) : ?>
						
								<?php if ( 'paid' == $status ) : ?>
									<?php $price = get_post_meta( get_the_ID(), 'image-price', true ); ?>
									<div class="price-credit"><em>$</em></div>
            			<div class="caption-price"><strong><?php echo $price ?></strong></div>
            <?php else : ?>
            		<span class="credit-free"><?php echo $status; ?></span>
								<?php endif; ?>
						<?php endif;
					?>
            </div>
					<?php $link_to = get_post_meta( get_the_ID(), 'image-link-to', true ); ?>
					<?php if ( $link_to && $link_to != '' ) : ?>
						<div class="image-link-to"><a href="<?php echo esc_url( $link_to ); ?>" target="_blank">i</a></div> 
					<?php endif; ?>
          </div>
				</div>
					<?php
					endwhile;
					}
					wp_reset_query();
					}
				?>
			</div>
		</div>
	</section>
    <div class="db-secondary sidebar-left col-md-3 col-sm-4 col-xs-12">
<?php get_sidebar('sidebar-1'); ?>
</div>
<?php wp_footer(); ?>

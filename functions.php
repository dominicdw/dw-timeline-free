<?php
require_once locate_template('/lib/init.php');            // Initial theme setup and constants
require_once locate_template('/lib/wrapper.php');         // Theme wrapper class
require_once locate_template('/lib/config.php');          // Configuration
require_once locate_template('/lib/titles.php');          // Page titles
require_once locate_template('/lib/nav.php');             // Custom nav modifications
require_once locate_template('/lib/gallery.php');         // Custom [gallery] modifications
require_once locate_template('/lib/comments.php');        // Custom comments modifications
require_once locate_template('/lib/scripts.php');         // Scripts and stylesheets
require_once locate_template('/lib/customizer.php');      // Customizer functions
require_once locate_template('/lib/custom.php');          // Custom functions
require_once locate_template('/category-meta.php');          // Custom functions
require_once locate_template('lib/db-category.php');          // Custom functions


function quantv_numeric_posts_nav( $query ) {



 //global $wp_query;

 /* Stop execution if there's only 1 page */
 if( $query->max_num_pages <= 1 )
  return;

 $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
 $max   = intval( $query->max_num_pages );

 /* Add current page to the array */
 if ( $paged >= 1 )
  $links[] = $paged;

 /* Add the pages around the current page to the array */
 if ( $paged >= 3 ) {
  $links[] = $paged - 1;
  $links[] = $paged - 2;
 }

 if ( ( $paged + 2 ) <= $max ) {
  $links[] = $paged + 2;
  $links[] = $paged + 1;
 }

 echo '<div class="navigation"><ul>' . "\n";

 /* Previous Post Link */
 if ( get_previous_posts_link() )
  printf( '<li>%s</li>' . "\n", get_previous_posts_link('<', '', 'yes') );

 /* Link to first page, plus ellipses if necessary */
 if ( ! in_array( 1, $links ) ) {
  $class = 1 == $paged ? ' class="active"' : '';

  printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

  if ( ! in_array( 2, $links ) )
   echo '<li>…</li>';
 }

 /* Link to current page, plus 2 pages in either direction if necessary */
 sort( $links );
 foreach ( (array) $links as $link ) {
  $class = $paged == $link ? ' class="active"' : '';
  printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
 }

 /* Link to last page, plus ellipses if necessary */
 if ( ! in_array( $max, $links ) ) {
  if ( ! in_array( $max - 1, $links ) )
   echo '<li>…</li>' . "\n";

  $class = $paged == $max ? ' class="active"' : '';
  printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
 }

 /* Next Post Link */
 if ( get_next_posts_link() )
  printf( '<li>%s</li>' . "\n", get_next_posts_link('>', '', 'yes')  );

 echo '</ul></div>' . "\n";

}

function db_unique_content( ) {

	$content = get_the_content();

	if ( preg_match('/\<img[^\>]*class="db-thumbnail".*\/>/', $content, $reg_matches ) ) {
		$db_content = $reg_matches[0];
		return $db_content;
	} else if ( has_post_thumbnail() ) {
		return get_the_post_thumbnail();
	} else {
		return;
	}
}

function dw_timeline_widgets_init() {
  register_sidebar( array(
    'name'          => esc_html__( 'Sidebar', 'dw-timeline' ),
    'id'            => 'sidebar-1',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );
}
add_action( 'widgets_init', 'dw_timeline_widgets_init' );

function dw_timeline_db_setting() {
  add_settings_field( 'db_page_setting', 'DB page categories', 'dw_timeline_db_setting_opiton', 'reading' );
  register_setting( 'reading', 'db_page_setting' );
}

add_action( 'admin_init', 'dw_timeline_db_setting');

function dw_timeline_db_setting_opiton() {
  $cats = get_option( 'db_page_setting' , false );
  echo '<input name="db_page_setting" id="db_page_setting" type="textbox" value="'.$cats.'" class="code" />';
}


function aw_custom_wp_list_categories( $args = '' ) {
  $defaults = array(
    'child_of'            => 0,
    'current_category'    => 0,
    'depth'               => 0,
    'echo'                => 1,
    'exclude'             => '',
    'exclude_tree'        => '',
    'feed'                => '',
    'feed_image'          => '',
    'feed_type'           => '',
    'hide_empty'          => 1,
    'hide_title_if_empty' => false,
    'hierarchical'        => true,
    'order'               => 'ASC',
    'orderby'             => 'name',
    'separator'           => '<br />',
    'show_count'          => 0,
    'show_option_all'     => '',
    'show_option_none'    => __( 'No categories' ),
    'style'               => 'list',
    'taxonomy'            => 'category',
    'title_li'            => __( 'Categories' ),
    'use_desc_for_title'  => 1,
  );

  $r = wp_parse_args( $args, $defaults );

  if ( !isset( $r['pad_counts'] ) && $r['show_count'] && $r['hierarchical'] )
    $r['pad_counts'] = true;

  // Descendants of exclusions should be excluded too.
  if ( true == $r['hierarchical'] ) {
    $exclude_tree = array();

    if ( $r['exclude_tree'] ) {
      $exclude_tree = array_merge( $exclude_tree, wp_parse_id_list( $r['exclude_tree'] ) );
    }

    if ( $r['exclude'] ) {
      $exclude_tree = array_merge( $exclude_tree, wp_parse_id_list( $r['exclude'] ) );
    }

    $r['exclude_tree'] = $exclude_tree;
    $r['exclude'] = '';
  }

  if ( ! isset( $r['class'] ) )
    $r['class'] = ( 'category' == $r['taxonomy'] ) ? 'categories' : $r['taxonomy'];

  if ( ! taxonomy_exists( $r['taxonomy'] ) ) {
    return false;
  }

  $show_option_all = $r['show_option_all'];
  $show_option_none = $r['show_option_none'];

  $categories = get_categories( $r );

  $output = '';
  if ( $r['title_li'] && 'list' == $r['style'] && ( ! empty( $categories ) || ! $r['hide_title_if_empty'] ) ) {
    $output = '<li class="' . esc_attr( $r['class'] ) . '">' . $r['title_li'] . '<ul>';
  }
  if ( empty( $categories ) ) {
    if ( ! empty( $show_option_none ) ) {
      if ( 'list' == $r['style'] ) {
        $output .= '<li class="cat-item-none">' . $show_option_none . '</li>';
      } else {
        $output .= $show_option_none;
      }
    }
  } else {
    if ( ! empty( $show_option_all ) ) {

      $posts_page = '';

      // For taxonomies that belong only to custom post types, point to a valid archive.
      $taxonomy_object = get_taxonomy( $r['taxonomy'] );
      if ( ! in_array( 'post', $taxonomy_object->object_type ) && ! in_array( 'page', $taxonomy_object->object_type ) ) {
        foreach ( $taxonomy_object->object_type as $object_type ) {
          $_object_type = get_post_type_object( $object_type );

          // Grab the first one.
          if ( ! empty( $_object_type->has_archive ) ) {
            $posts_page = get_post_type_archive_link( $object_type );
            break;
          }
        }
      }

      // Fallback for the 'All' link is the posts page.
      if ( ! $posts_page ) {
        if ( 'page' == get_option( 'show_on_front' ) && get_option( 'page_for_posts' ) ) {
          $posts_page = get_permalink( get_option( 'page_for_posts' ) );
        } else {
          $posts_page = home_url( '/' );
        }
      }

      $posts_page = esc_url( $posts_page );
      if ( 'list' == $r['style'] ) {
        $custom_current_category = isset( $_GET['cat-id'] ) ? $_GET['cat-id'] : null;
        if ( !$custom_current_category ) {
          $output .= '<li class="cat-item-all category-active">';
        } else {
          $output .= '<li class="cat-item-all">';
        }
        $all = get_option( 'db_page_setting' , false );
        $output .= '<a href="http://blog.designbold.com/designbold-template/">'.$show_option_all.'</a>';
        if ( $all && $all != '' ) {
          $db_categories = explode( ',' , $all );
          $db_count = 0;
          foreach ( $db_categories  as $db_categorie ) {
            $db_cat = get_category( $db_categorie );
            $count = $db_cat->category_count;
            $db_count = intval( $db_count ) + intval( $count );
          }
          $output .= ' <span class="badge">'.$db_count.'</span>';
        } else {
          $posts_count = wp_count_posts();
          $output .= ' <span class="badge">'.$posts_count->publish.'</span>';
        }
        $output .= '</li>';
      } else {
        $output .= "<a href='$posts_page'>$show_option_all</a>";
      }
    }

    if ( empty( $r['current_category'] ) && ( is_category() || is_tax() || is_tag() ) ) {
      $current_term_object = get_queried_object();
      if ( $current_term_object && $r['taxonomy'] === $current_term_object->taxonomy ) {
        $r['current_category'] = get_queried_object_id();
      }
    }

    if ( $r['hierarchical'] ) {
      $depth = $r['depth'];
    } else {
      $depth = -1; // Flat.
    }
    $output .= walk_category_tree( $categories, $depth, $r );
  }

  if ( $r['title_li'] && 'list' == $r['style'] )
    $output .= '</ul></li>';

  /**
   * Filter the HTML output of a taxonomy list.
   *
   * @since 2.1.0
   *
   * @param string $output HTML output.
   * @param array  $args   An array of taxonomy-listing arguments.
   */
  $html = apply_filters( 'wp_list_categories', $output, $args );

  if ( $r['echo'] ) {
    echo $html;
  } else {
    return $html;
  }
}

<?php
/**
 * 
 *
 * 
 * @since Timeline Pro 1.0.9
 */

if ( ! function_exists( 'dw_timeline_pro_category_generate_html_add_form_fields' ) ) {
  function dw_timeline_pro_category_generate_html_add_form_fields(){
    ?>
    <div class="form-field">
      <label><?php _e( 'Category Header Image', 'dw-timeline-pro' ); ?></label>
      <div id="category_header_image" style="float:left;margin-right:10px;"><img src="" width="60px" height="60px" /></div>
      <div style="line-height:60px;">
        <input type="hidden" id="category_header_image_id" name="category_header_image_id" />
        <button type="button" class="upload_image_button button"><?php _e( 'Upload/Add image', 'dw-timeline-pro' ); ?></button>
        <button type="button" class="remove_image_button button"><?php _e( 'Remove image', 'dw-timeline-pro' ); ?></button>
      </div>
    </div>
  <?php
  }
  add_action( 'category_add_form_fields', 'dw_timeline_pro_category_generate_html_add_form_fields' );
}

if ( ! function_exists( 'dw_timeline_pro_category_generate_html_edit_form_fields' ) ) {
  function dw_timeline_pro_category_generate_html_edit_form_fields( $tag ){
    $options = dw_timeline_pro_get_category_option( $tag->term_id );
    if ( $options['header_image'] ) {
      $image = wp_get_attachment_thumb_url( $options['header_image'] );
    }else {
      $image = '';
    }
    ?>
    <tr class="form-field">
      <th scope="row" valign="top">
        <label for="tag-style">
          <?php _e( 'Category Header Image','dw-timeline-pro' ) ?>
        </label>
      </th>
      <td>
        <div class="form-field">
          <div id="category_header_image" style="float:left;margin-right:10px;"><img src="<?php echo esc_url( $image );?>" width="60px" height="60px" /></div>
          <div style="line-height:60px;">
            <input type="hidden" id="category_header_image_id" name="category_header_image_id" value="<?php echo esc_attr( $options['header_image'] ) ? esc_attr( $options['header_image'] ) : '';?>" />
            <button type="button" class="upload_image_button button"><?php _e( 'Upload/Add image', 'dw-timeline-pro' ); ?></button>
            <button type="button" class="remove_image_button button"><?php _e( 'Remove image', 'dw-timeline-pro' ); ?></button>
          </div>
        </div>
      </td>
    </tr>
    <?php
  }
  add_action( 'category_edit_form_fields', 'dw_timeline_pro_category_generate_html_edit_form_fields' );
}

if ( ! function_exists( 'dw_timeline_pro_enqueue_thickbox_for_edit_tag_page' ) ) {
  function dw_timeline_pro_enqueue_thickbox_for_edit_tag_page(){
    global $pagenow;

    if ( 'edit-tags.php' == $pagenow || 'term.php' == $pagenow ) {
      wp_enqueue_media();

      wp_register_script( 'dw-category-upload', get_template_directory_uri().'/assets/js/edit-tags.js', array( 'jquery', 'media-upload', 'thickbox' ) );

      wp_enqueue_script( 'dw-category-upload' );
    }
  }
  add_action( 'admin_print_scripts', 'dw_timeline_pro_enqueue_thickbox_for_edit_tag_page' );
}

if ( ! function_exists( 'dw_timeline_pro_save_category_option' ) ) {
  function dw_timeline_pro_save_category_option( $category_id ){
    $category_options = array();
    if ( isset( $_POST['tag-style'] ) ) {
      $category_options['style'] = sanitize_html_class( $_POST['tag-style'] );
    }

    if ( isset( $_POST['category_header_image_id'] ) ) {
      $category_options['header_image'] = sanitize_key( $_POST['category_header_image_id'] );
    }
    if ( ! empty( $category_options ) ) {
      update_option( 'dw_timeline_pro_category_option_'.$category_id, $category_options );
    }
  }
  add_action( 'create_category', 'dw_timeline_pro_save_category_option' );
  add_action( 'edit_category', 'dw_timeline_pro_save_category_option' );
}

if ( ! function_exists( 'dw_timeline_pro_get_category_option' ) ) {
  function dw_timeline_pro_get_category_option( $category_id ){
    return get_option( 'dw_timeline_pro_category_option_'.$category_id, array(
      'header_image'          => '',
      ) );
  }
}
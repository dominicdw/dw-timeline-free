jQuery(function($){
   $('#db-custom-search-box input').keypress( function(e){
        if ( e.which == 13 ) {
            e.preventDefault();
            var t = $(this);
            var q = t.val();
            var category = $('#hidden-db-category').val();

            var page = t.parent().data('page');
            if ( category == 0  ) {
                window.location = page+'?q='+q;
            } else {
                window.location = page+'?cat-id='+category+'&q='+q;
            }

        }
    })
});
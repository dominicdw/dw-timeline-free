<?php
/**
 * Template Name: Design Bold category
 *
 */
 ?>
<?php
	$category = isset( $_GET['cat-id'] ) ? $_GET['cat-id'] : 0;
	$keyword = isset( $_GET['q'] ) ? $_GET['q'] : null;
?>

<div class="row-header">
<div class="col-md-8 col-sm-12">
  <h2>DESIGNBOLD TEMPLATE</h2>
    <div class="sitemaps">
     <?php
if ( function_exists('yoast_breadcrumb') ) {
     yoast_breadcrumb('<p id="breadcrumbs">','</p>');
}
?>
    </div>
</div>
<div class="col-md-4 col-sm-12">
  <div class="help-search">
<div id="db-custom-search-box" class="col-md-12" data-page=<?php echo " http://blog.designbold.com/templates/"; ?>><input id="db-custom-search-input" style="" type="text" value="<?php echo $keyword; ?>" placeholder="Search..."><i id="db-custom-search-submit" aria-hidden="true"></i></div>
<input type="hidden" id="hidden-db-category" value="<?php echo $category; ?>">
                            </div>
                        </div>
                    </div>

<section id="primary" class="db-primary site-content">

	
	<div id="content" role="main" class="col-md-9 col-sm-8 col-xs-12">
		<?php
			$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
			$current_page = $_SERVER['HTTP_HOST'] . $uri_parts[0];
		?>
		
	<?php
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$posts_per_page = get_option('posts_per_page');

	$db = get_option( 'db_page_setting' , false );
	
	if ( $category ) {
		$cat = explode( ',' , $category );
	}  else if ( $db && $db != '' ) {
		$cat = explode( ',' , $db );
	}


	$args = array(
		'posts_per_page' => $posts_per_page,
		'paged' => $paged,
		'post_status' => 'publish',
		'post_type' => 'post',
		'cat' => $cat,
		's' => $keyword
	);

	$query = new WP_Query( $args );
	if ( $query->have_posts()) : ?>
		<div class="db-image-gallery">
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<div class="col-md-4 template-item">
					<div class="entry-thumbnail db-thumbnail">
						<a href="http://blog.designbold.com/template-item/?post-id=<?php echo get_the_ID(); ?>"><?php echo db_unique_content(); ?></a>
					
					<div class="db-title entry-title"><?php echo get_the_title(); ?></div>
          <div class="entry-status">
					<?php $status = get_post_meta( get_the_ID(), 'image-status', true );
						if ( $status && $status != '' ) : ?>
						
								<?php if ( 'paid' == $status ) : ?>
									<?php $price = get_post_meta( get_the_ID(), 'image-price', true ); ?>
									<div class="price-credit"><em>$</em></div>
            			<div class="caption-price"><strong><?php echo $price ?></strong></div>
            <?php else : ?>
            		<span class="credit-free"><?php echo $status; ?></span>
								<?php endif; ?>
						<?php endif;
					?>
            </div>
					<?php $link_to = get_post_meta( get_the_ID(), 'image-link-to', true ); ?>
					<?php if ( $link_to && $link_to != '' ) : ?>
						<div class="image-link-to"><a href="<?php echo esc_url( $link_to ); ?>" target="_blank">i</a></div> 
					<?php endif; ?>
          </div>
				</div>
			<?php endwhile; ?>
		</div>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
    <nav class="db-navigation" aria-label="Page navigation">
      <ul class="pagination">
        <?php quantv_numeric_posts_nav( $query ); ?>
      </ul>
    </nav>
    <?php wp_reset_query(); ?>
		</div>
    <div class="db-secondary sidebar-left col-md-3 col-sm-4 col-xs-12">
<?php get_sidebar('sidebar-1'); ?>
</div>
	</section>
<?php wp_footer(); ?>

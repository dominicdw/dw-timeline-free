<?php

class DB_Widget_Categories_List_Custom extends WP_Widget {
  public function __construct() {
    $widget_ops = array( 'classname' => 'db_widget_categories', 'description' => __( 'Show a list of categories or tags.', 'db-widget' ) );
    parent::__construct( 'db-categories-custom', __( 'Custom Category Widget', 'db-widget' ), $widget_ops );
  }

  public function widget( $args, $instance ) {
    $title = isset( $instance['title'] ) ? $instance['title'] : __( 'Categories', 'db-widget' );
    $show_count = isset( $instance['show_count'] ) ? $instance['show_count'] : false;
    $hide_empty = isset( $instance['hide_empty'] ) ? $instance['hide_empty'] : false;
    $dropdown = isset( $instance['dropdown'] ) ? $instance['dropdown'] : false;
    $query = array();
    $query['hide_empty'] = 'on' == $hide_empty ? true : false;
    $taxonomy = 'category';
    $question_categories = get_terms( $taxonomy, $query );
    extract( $args );

    echo $before_widget;
    echo $before_title;
    echo $title;
    echo $after_title;
    if ( $question_categories ) {
      if ( !$dropdown ) {

        if ( $show_count != false ) {
          $show_count_no = 1;
        } else {
          $show_count_no = 0;
        }

        if ( $hide_empty != false ) {
          $hide_empty_no = 1;
        } else {
          $hide_empty_no = 0;
        }

        $args = array(
          // 'depth' => 4,
          'show_option_all'    => 'All',
          'show_option_none'   => '',
          'echo' => 1,
          // 'include' => array( 1,5 ),
          'hierarchical' => 1,
              'taxonomy' => $taxonomy,
              'title_li' => '',
              'hide_empty'  => $hide_empty_no,
              'show_option_none'  => __( 'No categories' ),
              'walker' => new AW_Walker_Category,
              'show_count' => $show_count_no,
          );

        $cat_ids = get_option( 'db_page_setting' , false );
        if ( $cat_ids && $cat_ids != '' ) {
          $categories = get_terms( array(
            'taxonomy' => 'category',
          ) );

          $cat_arr = array();
          foreach ( $categories  as $category ) {
            $cat_arr[] = $category->term_id;
          }
          $include_cats = explode( ',' , $cat_ids );
          $exclude_arr = array_diff($cat_arr, $include_cats );
          $args['exclude'] = $exclude_arr;
        }

        echo '<ul>';
          aw_custom_wp_list_categories( $args );
        echo '</ul>';
      } else {

        $args = array(
        'show_option_all'    => 'All',
        'show_option_none'   => '',
        'option_none_value'  => '-1',
        'orderby'            => 'name',
        'order'              => 'ASC',
        'show_count'         => 0,
        'hide_empty'         => 0,
        'echo'               => 1,
        'selected'           => 0,
        'hierarchical'       => 1,
        'name'               => 'cat',
        'id'                 => '',
        'class'              => 'postform',
        // 'depth'              => 0,
        'tab_index'          => 0,
        'taxonomy'           => 'category',
        'hide_if_empty'      => false,
        'value_field'      => 'term_id',
      );

        wp_dropdown_categories( $args );
/*        echo '<select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">';
          foreach( $question_categories as $cat ) {
            echo '<option value="'.get_term_link( $cat->term_id ).'">'.$cat->name;
            if ( $show_count ) {
              echo '&#32;&#40;'. intval( $cat->count ) .'&#41;';
            }
            echo '</option>';
          }
        echo '</select>';*/
      }
    }
    echo $after_widget;
  }

  public function update( $new_instance, $old_instance ) {
    return $new_instance;
  }

  public function form( $instance ) {
    $title = isset( $instance['title'] ) ? $instance['title'] : '';
    $show_count = isset( $instance['show_count'] ) ? $instance['show_count'] : false;
    $hide_empty = isset( $instance['hide_empty'] ) ? $instance['hide_empty'] : false;
    $dropdown = isset( $instance['dropdown'] ) ? $instance['dropdown'] : false;
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ) ?>">
        <?php _e( 'Title', 'db-widget' ) ?>
        <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ) ?>" name="<?php echo $this->get_field_name( 'title' ) ?>" value="<?php echo $title ?>" >
      </label>
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'show_count' ) ?>">
        <input type="checkbox" id="<?php echo $this->get_field_id( 'show_count' ) ?>" name="<?php echo $this->get_field_name( 'show_count' ) ?>" <?php checked( $show_count, 'on' ) ?>>
        <?php _e( 'Show count', 'dwqa' ); ?>
      </label>
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'hide_empty' ) ?>">
        <input type="checkbox" id="<?php echo $this->get_field_id( 'hide_empty' ) ?>" name="<?php echo $this->get_field_name( 'hide_empty' ) ?>" <?php checked( $hide_empty, 'on' ) ?>>
        <?php _e( 'Hide Empty Categories/Tags', 'dwqa' ) ?>
      </label>
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'dropdown' ) ?>">
        <input type="checkbox" id="<?php echo $this->get_field_id( 'dropdown' ) ?>" name="<?php echo $this->get_field_name( 'dropdown' ) ?>" <?php checked( $dropdown, 'on' ) ?>>
        <?php _e( 'Display as dropdown', 'dwqa' ) ?>
      </label>
    </p>
    <?php
  }
}
add_action( 'widgets_init', create_function( '', "register_widget('DB_Widget_Categories_List_Custom');" ) );

class AW_Walker_Category extends Walker_Category {
  /**
   * What the class handles.
   *
   * @since 2.1.0
   * @access public
   * @var string
   *
   * @see Walker::$tree_type
   */
  public $tree_type = 'category';

  /**
   * Database fields to use.
   *
   * @since 2.1.0
   * @access public
   * @var array
   *
   * @see Walker::$db_fields
   * @todo Decouple this
   */
  public $db_fields = array ('parent' => 'parent', 'id' => 'term_id');

  /**
   * Starts the list before the elements are added.
   *
   * @since 2.1.0
   * @access public
   *
   * @see Walker::start_lvl()
   *
   * @param string $output Used to append additional content. Passed by reference.
   * @param int    $depth  Optional. Depth of category. Used for tab indentation. Default 0.
   * @param array  $args   Optional. An array of arguments. Will only append content if style argument
   *                       value is 'list'. See wp_list_categories(). Default empty array.
   */
  public function start_lvl( &$output, $depth = 0, $args = array() ) {
    if ( 'list' != $args['style'] )
      return;

    $indent = str_repeat("\t", $depth);
    $output .= "$indent<ul class='children'>\n";
  }

  /**
   * Ends the list of after the elements are added.
   *
   * @since 2.1.0
   * @access public
   *
   * @see Walker::end_lvl()
   *
   * @param string $output Used to append additional content. Passed by reference.
   * @param int    $depth  Optional. Depth of category. Used for tab indentation. Default 0.
   * @param array  $args   Optional. An array of arguments. Will only append content if style argument
   *                       value is 'list'. See wp_list_categories(). Default empty array.
   */
  public function end_lvl( &$output, $depth = 0, $args = array() ) {
    if ( 'list' != $args['style'] )
      return;

    $indent = str_repeat("\t", $depth);
    $output .= "$indent</ul>\n";
  }

  /**
   * Starts the element output.
   *
   * @since 2.1.0
   * @access public
   *
   * @see Walker::start_el()
   *
   * @param string $output   Passed by reference. Used to append additional content.
   * @param object $category Category data object.
   * @param int    $depth    Optional. Depth of category in reference to parents. Default 0.
   * @param array  $args     Optional. An array of arguments. See wp_list_categories(). Default empty array.
   * @param int    $id       Optional. ID of the current category. Default 0.
   */
  public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
    /** This filter is documented in wp-includes/category-template.php */
    $cat_name = apply_filters(
      'list_cats',
      esc_attr( $category->name ),
      $category
    );

    // Don't generate an element if the category name is empty.
    if ( ! $cat_name ) {
      return;
    }

    $line = '';
    $max_line = intval( $depth );
    for ( $i=0; $i < $max_line; $i++ ) { 
      $line .= '— ';
    }

    $link = '<a href="http://blog.designbold.com/template/?cat-id=' . $category->term_id . '" ';
    if ( $args['use_desc_for_title'] && ! empty( $category->description ) ) {
      /**
       * Filter the category description for display.
       *
       * @since 1.2.0
       *
       * @param string $description Category description.
       * @param object $category    Category object.
       */
      $link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';
    }

    $link .= '>';
    $link .= $line;
    $link .= $cat_name . '</a>';

    if ( ! empty( $args['feed_image'] ) || ! empty( $args['feed'] ) ) {
      $link .= ' ';

      if ( empty( $args['feed_image'] ) ) {
        $link .= '(';
      }

      $link .= '<a href="' . esc_url( get_term_feed_link( $category->term_id, $category->taxonomy, $args['feed_type'] ) ) . '"';

      if ( empty( $args['feed'] ) ) {
        $alt = ' alt="' . sprintf(__( 'Feed for all posts filed under %s' ), $cat_name ) . '"';
      } else {
        $alt = ' alt="' . $args['feed'] . '"';
        $name = $args['feed'];
        $link .= empty( $args['title'] ) ? '' : $args['title'];
      }

      $link .= '>';

      if ( empty( $args['feed_image'] ) ) {
        $link .= $name;
      } else {
        $link .= "<img src='" . $args['feed_image'] . "'$alt" . ' />';
      }
      $link .= '</a>';

      if ( empty( $args['feed_image'] ) ) {
        $link .= ')';
      }
    }

    if ( ! empty( $args['show_count'] ) ) {
      $link .= ' <span class="badge">' . number_format_i18n( $category->count ) . '</span>';
    }
    if ( 'list' == $args['style'] ) {
      $output .= "\t<li";
      $css_classes = array(
        'cat-item',
        'cat-item-' . $category->term_id,
      );

      $custom_current_category = isset( $_GET['cat-id'] ) ? $_GET['cat-id'] : null; 
      if ( $custom_current_category && $category->term_id == $custom_current_category  ) {
        $css_classes[] = 'category-active';
      }
     
      if ( ! empty( $args['current_category'] ) ) {
        // 'current_category' can be an array, so we use `get_terms()`.
        $_current_terms = get_terms( $category->taxonomy, array(
          'include' => $args['current_category'],
          'hide_empty' => false,
        ) );
        foreach ( $_current_terms as $_current_term ) {
          if ( $category->term_id == $_current_term->term_id ) {
            $css_classes[] = 'current-cat';
          } elseif ( $category->term_id == $_current_term->parent ) {
            $css_classes[] = 'current-cat-parent';
          }
          while ( $_current_term->parent ) {
            if ( $category->term_id == $_current_term->parent ) {
              $css_classes[] =  'current-cat-ancestor';
              break;
            }
            $_current_term = get_term( $_current_term->parent, $category->taxonomy );
          }
        }
      }

      /**
       * Filter the list of CSS classes to include with each category in the list.
       *
       * @since 4.2.0
       *
       * @see wp_list_categories()
       *
       * @param array  $css_classes An array of CSS classes to be applied to each list item.
       * @param object $category    Category data object.
       * @param int    $depth       Depth of page, used for padding.
       * @param array  $args        An array of wp_list_categories() arguments.
       */
      $css_classes = implode( ' ', apply_filters( 'category_css_class', $css_classes, $category, $depth, $args ) );

      $output .=  ' class="' . $css_classes . '"';
      $output .= ">$link\n";
    } elseif ( isset( $args['separator'] ) ) {
      $output .= "\t$link" . $args['separator'] . "\n";
    } else {
      $output .= "\t$link<br />\n";
    }
  }

  /**
   * Ends the element output, if needed.
   *
   * @since 2.1.0
   * @access public
   *
   * @see Walker::end_el()
   *
   * @param string $output Passed by reference. Used to append additional content.
   * @param object $page   Not used.
   * @param int    $depth  Optional. Depth of category. Not used.
   * @param array  $args   Optional. An array of arguments. Only uses 'list' for whether should append
   *                       to output. See wp_list_categories(). Default empty array.
   */
  public function end_el( &$output, $page, $depth = 0, $args = array() ) {
    if ( 'list' != $args['style'] )
      return;

    $output .= "</li>\n";
  }
}



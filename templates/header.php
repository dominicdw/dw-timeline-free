<?php

  $class = 'no-cover';
  if ( is_front_page() || is_archive() || is_search() || is_home() || is_page_template('page-template-db.php') || is_page_template('page-template-visual.php') || is_page_template( 'page-designbold-category.php' ) || is_page_template( 'page-designbold-single.php' ) ) {
    $class = 'cover';
  }

  if ( is_single() ) {
    $post_thumbnail_url = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );
    if ( ! empty($post_thumbnail_url) ) {
      $class = 'cover';
      ?>
      <style>
        .banner.cover {
          background-image: url( <?php echo $post_thumbnail_url; ?> );
        }
      </style>
      <?php
    }
  }
?>

<header class="banner <?php echo $class ?>" role="banner">
  <div class="header-inner">
      <nav class="nav-main nav-main navbar-fixed-top navbar-main" role="navigation">
        <div class="container">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
            <i class="glyphicon glyphicon-align-justify"></i>
          </button>
          <!-- Change logo in the timeline theme -->
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="navbar-brand">
          <?php $site_logo = dw_timeline_get_theme_option( 'site_logo' ); ?>
          <?php if ( $site_logo ) : ?>
            <img class="logo-white" src="<?php echo esc_url( $site_logo ); ?>" title="<?php bloginfo(' name' ); ?>">
          <?php else : ?>
<?php endif; ?>
             <?php $site_logo = dw_timeline_get_theme_option( 'site_logo2' ); ?>
          <?php if ( $site_logo ) : ?>
            <img class="logo-gray" src="<?php echo esc_url( $site_logo ); ?>" title="<?php bloginfo(' name' ); ?>">
          <?php else : ?>
          <?php endif; ?>
        </a>
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav collapse navbar-collapse navbar-main-collapse'));
          endif;
        ?>
        </div>
      </nav>
      <?php if( is_front_page() || is_archive() || is_home() || is_page_template( 'page-designbold-category.php' ) || is_page_template('page-template-db.php') || is_page_template('page-template-visual.php') || is_page_template( 'page-designbold-single.php' ) ) : ?>
        <hgroup>
          <div class="container">
             <h1 class="page-title">
          <?php if ( is_page_template( 'page-designbold-category.php' ) || is_page_template( 'page-designbold-single.php' ) ) {
            $category = isset( $_GET['cat-id'] ) ? $_GET['cat-id'] : null;
            $post_id = isset( $_GET['post-id'] ) ? $_GET['post-id'] : null;
             if ( is_page_template( 'page-designbold-category.php' )  ) {
                if ( $category ) {
                  $cat = get_category( $category );
                  echo $cat->name;
                } else {
                  echo dw_timeline_title();
                }
              } elseif ( is_page_template( 'page-designbold-single.php' ) ) {
                if ( $post_id ) {
                   $categories = wp_get_post_categories( $post_id );
                    if ( $categories ) {
                      $category_id = $categories[0];
                      $cat = get_category( $category_id );
                       echo $cat->name;
                    } else {
                      echo dw_timeline_title();
                    }
                } else {
                  echo dw_timeline_title();
                }
             } else {
              echo dw_timeline_title();
             }
          } else { ?>
              <?php echo dw_timeline_title(); ?>
            <?php } ?>
            </h1>
            <h2 class="page-description">
            <?php if ( is_page_template( 'page-designbold-category.php' ) || is_page_template( 'page-designbold-single.php' ) ) {
              $category_x = isset( $_GET['cat-id'] ) ? $_GET['cat-id'] : null;
              $post_id_x = isset( $_GET['post-id'] ) ? $_GET['post-id'] : null;
              $post_desc =  get_post_meta( get_the_ID() , 'page_desc', true );
              if ( is_page_template( 'page-designbold-category.php' )  ) {
                if ( $category_x ) {
                  $cat = get_category( $category_x );
                  echo $cat->description;
                } elseif ( $post_desc ) {
                  echo $post_desc;
                }
              } else if ( is_page_template( 'page-designbold-single.php' ) ) {
                if ( $post_id_x ) {
                  $categories = wp_get_post_categories( $post_id_x );
                  if ( $categories ) {
                    $category_id = $categories[0];
                    $cat = get_category( $category_id );
                     echo $cat->description;
                  } elseif ( $post_desc ) {
                    echo $post_desc;
                  }
                }
              }
            }
            else { ?>
             <?php bloginfo('description'); ?>
            <?php } ?>
            </h2>
            <?php if ( is_page_template( 'page-designbold-category.php' ) || is_page_template( 'page-designbold-single.php' ) ) {
              } else { ?>
               <button id="get-started" class="btn btn-default btn-coner"><?php echo dw_timeline_get_theme_option('get_start','Get Start Now') ?></button>
            <?php } ?>
          </div>
        </hgroup>
      <?php elseif( is_search() ) : ?>
        <div class="container">
          <h1 class="page-title">
            <?php echo dw_timeline_title(); ?>
          </h1>
        </div>
      <?php endif; ?>
  </div>
</header>

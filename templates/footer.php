</div>
<!--FOOTER-->
<div style="clear:both;"></div>
<div id="footer">
    <div class="wrapper">
    	<div class="foot">
            <div class="section footer">
<div class="container">
<div class="footer-nav">
<div class="row">
<div class="col-md-3 col-sm-12">
<a href="https://www.designbold.com/" class="logo-footer">
<img alt="" src="http://blog.designbold.com/wp-content/uploads/2016/07/logo_white.png" style="
    width: 165px;
    height: 35px;
">
</a>
<div class="social">
<a href="https://www.facebook.com/Designbold-1472142493094093/" target="_blank">
<i class="fa fa-facebook-square"></i>
</a>
<a href="https://twitter.com/_Designbold" target="_blank">
<i class="fa fa-twitter-square"></i>
</a>
<a href="https://www.pinterest.com/designbold/" target="_blank">
<i class="fa fa-pinterest-square"></i>
</a>
<a href="https://www.instagram.com/designbold/" target="_blank">
<i class="fa fa-instagram"></i>
</a>
</div>
</div>
<div class="col-md-9  col-sm-12">
<ul class="ul-footer">
<li>
<h4> <a href="https://www.designbold.com/about" data-stay="true">ABOUT</a></h4>
<ul class="sub_menu_footer">
<li><a href="https://www.designbold.com/about" data-stay="true"> About Us</a></li>
<li><a href="https://www.designbold.com/careers" data-stay="true">Our Team</a></li>
<li><a href="https://www.designbold.com/contributor" data-stay="true">Contribute</a></li>
<li><a href="https://www.designbold.com/pricing" data-stay="true">Pricing</a></li>
</ul>
</li>
<li>
<h4> <a href="https://www.designbold.com/tutorials" data-stay="true"> PRODUCTS</a></h4>
<ul class="sub_menu_footer">
<li><a href="https://www.designbold.com/design/button" data-stay="true">DesignIt Button</a></li>
<li><a href="https://www.designbold.com/brand-kit">Brand Kits</a></li>
</ul>
</li>
<li>
<h4> <a href="https://www.designbold.com/support" data-stay="true">SUPPORT </a></h4>
<ul class="sub_menu_footer">
<li><a href="https://www.designbold.com/term" data-stay="true">Terms of Use</a></li>
<li><a href="https://www.designbold.com/policy" data-stay="true"> Privacy Policy</a></li>
<li><a href="https://www.designbold.com/support" data-stay="true">Support Center</a></li>
<li><a href="https://www.designbold.com/contact" data-stay="true">Contact Us</a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="footer-coppyright">
<p class="text-center">© 2016 Copyright DesignBold <i class="fa fa-heart" style="color:#ee0d22"></i></p>
</div>
</div>
        </div>
    </div>
</div>

</div>
<?php wp_footer(); ?>
</body>

</html>